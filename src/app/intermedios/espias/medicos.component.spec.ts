import { MedicosComponent } from './medicos.component';
import { MedicosService } from './medicos.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/throw';


describe('Medicos Component', () => {

    let componente: MedicosComponent;
    const servicio = new MedicosService(null);

    beforeEach( () => {
        componente = new MedicosComponent(servicio);
    });

    it('Init: Debe de cargar los medicos', () => {

        const medicos = ['Medico1', 'Medico3', 'Medico2'];

        spyOn( servicio, 'getMedicos' ).and.callFake( () => {

            return Observable.from( [ medicos ] );

        });

        componente.ngOnInit();

        expect( componente.medicos.length ).toBeGreaterThan(0);
   
    });

    it('Debe de llamar al servidor, para agregar un nuevo medico', () => {

        const espia = spyOn(servicio, 'agregarMedico').and.callFake( res => {
            return Observable.empty();
        });

        componente.agregarMedico();

        expect( espia ).toHaveBeenCalled();

    });

    it('Debe de agregar un nuevo médico al arreglo de médicos', () => {

        const medico = {
            id: 1,
            nombre: 'Benja'
        }

        spyOn( servicio, 'agregarMedico' ).and.returnValue( Observable.from([ medico ]) );

        componente.agregarMedico();

        expect( componente.medicos.indexOf( medico ) ).toBeGreaterThanOrEqual(0);

    });

    it('Debe de contener al menos un medico', () => {

        const medico = {
            id: 1,
            nombre: 'Benja'
        }

        spyOn( servicio, 'agregarMedico' ).and.returnValue( Observable.from([ medico ]) );

        componente.agregarMedico();

        expect( componente.medicos.length ).toBeGreaterThanOrEqual(1);

    });

    it('Si falla la adicion, la propiedad mensajeError, debe ser igual al error del servicio', () => {

        const miError = 'No se puedo agregar el medico';
        spyOn( servicio, 'agregarMedico' ).and.returnValue( Observable.throw( miError ) );

        componente.agregarMedico(); 

        expect( componente.mensajeError ).toBe( miError );
       
    });

    it('Debe llamar al servidor para borrar un médico', () => {



        spyOn( window, 'confirm').and.returnValue(true);

        const espia = spyOn( servicio, 'borrarMedico' ).and.returnValue( Observable.empty() );

        componente.borrarMedico('1');

        expect( espia ).toHaveBeenCalledWith('1');// sirve para ver si fue llamado con algún argumento

    });

    it('No debe llamar al servidor para borrar un médico', () => {

        let mensaje = 'hola';

        spyOn( window, 'confirm').and.returnValue(false);

        const espia = spyOn( servicio, 'borrarMedico' ).and.returnValue( Observable.empty() );

        componente.borrarMedico('1');
        
        expect( mensaje ).toBe('hola');
        expect( espia ).not.toHaveBeenCalledWith('1');// el not, es para hacer la negacion a esa accion;

    });

});
