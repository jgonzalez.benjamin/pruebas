import { EventEmitter } from "@angular/core";



export class Jugador2{

    hp:number;
    hpCambia = new EventEmitter<number>();   
    armas:any[];
    nick:string;

    constructor(){
        this.hp = 100;
        this.nick = 'Jugador 2';
        this.armas = ['sin armas'];
    }
    recibeDanio( danio: number ){
        if (danio >= this.hp) {
            this.hp = 0;
        }else{
            this.hp = this.hp - danio;
        }
        this.hpCambia.emit( this.hp );
        
    }
    setNick( nick:string ){
        this.nick = nick;
    }
    setArmas( armas:any[] ){
        this.armas.push( armas );
    }
    getNick():string {
        return this.nick;
    }

}