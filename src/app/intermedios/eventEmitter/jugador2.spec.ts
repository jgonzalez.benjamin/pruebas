import { Jugador2 } from "./jugador2";

describe( 'Jugador 2 Emit', () => {
    let jugador: Jugador2;

    beforeEach( () => {
        jugador = new Jugador2()
    });

    it('Debe de emitir un evento, si se recibe daño', () => {

        let newHp = 0;

        jugador.hpCambia.subscribe( hp => {
            newHp = hp;
        });

        jugador.recibeDanio(1000);

        expect( newHp ).toBe(0);


    });
    it('Debe de emitir un evento, si se recibe daño y sobrevivir, si es menos de 100', () => {

        let newHp = 0;

        jugador.hpCambia.subscribe(hp => newHp = hp);

        jugador.recibeDanio(99);

        expect( newHp ).toBe(1);


    });

});