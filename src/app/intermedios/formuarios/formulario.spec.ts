import { FormularioRegister } from "./formulario";
import { FormBuilder } from "@angular/forms";


describe('Formularios', () => {

    let component: FormularioRegister;

    beforeEach( () => {
        component = new FormularioRegister( new FormBuilder() );
    });

    it('Debe de crear un formulario con dos campos', () => {

        expect( component.form.contains('email') ).toBeTruthy();
        expect( component.form.contains('password') ).toBeTruthy();

    });

    it('El campo email, debe ser obligatorio', () => {

        const control = component.form.get('email');

        control.setValue('');
        expect( control.valid ).toBeFalsy();

    });
    
    it('El campo email, debe ser un email válido', () => {

        const control = component.form.get('email');

        control.setValue('perde.-@bbto.govir');
        expect( control.valid ).toBeTruthy();

    });

})