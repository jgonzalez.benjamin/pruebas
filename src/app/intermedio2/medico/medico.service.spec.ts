import { TestBed, inject } from '@angular/core/testing';

import { MedicoService } from './medico.service';
import { HttpClientModule } from '@angular/common/http';

describe('MedicoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MedicoService],
      imports: [HttpClientModule]
    });
  });

  it('should be created', inject([MedicoService], (service: MedicoService) => {
    expect(service).toBeTruthy();
  }));
});
