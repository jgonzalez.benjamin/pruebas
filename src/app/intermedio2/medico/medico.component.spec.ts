import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicoComponent } from './medico.component';
import { MedicoService } from './medico.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('MedicoComponent', () => {
  let component: MedicoComponent;
  let fixture: ComponentFixture<MedicoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({  // el TestBed se confuga por lo general en el beforeEch();
      declarations: [ MedicoComponent ], 
      providers: [ MedicoService, HttpClient ],
      imports: [ HttpClientModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicoComponent);// crea el componente que vamos a probar
    component = fixture.componentInstance;// Crea la instacia del componente
    fixture.detectChanges();// esta al pendiente de cambios del copmente 
  });

  it('Debe de crearse el componente correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Debe de retornar el nombre del médico', () => {
    const nombre = 'Benja';
    const mensaje = component.saludarMedico(nombre);

    expect(mensaje).toContain(nombre);
  });

});
