import { IncrementadorComponent } from "./incrementador.component";
import { COMPOSITION_BUFFER_MODE } from "@angular/forms";


describe('Incrementador Component Unit', () => {

    let component: IncrementadorComponent;

    beforeEach( ()=> {
        component = new IncrementadorComponent();
    })

    it('No de be de pasar de 100 el progreso', () => {

        component.progreso = 50; 

        component.cambiarValor(5);

        expect( component.progreso ).toBe(55);

    });


});