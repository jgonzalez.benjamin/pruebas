import { getRobots } from "./arreglos";

describe('Pruebas de Arreglos', () => {

    it('Debe retornar al menos 3 roots', () => {

        const resp = getRobots();
        expect( resp.length ).toBeGreaterThanOrEqual(3);

    });
    it('Debe de contener a MegaMan y Ultron', () => {

        const resp = getRobots();
        
        expect( resp ).toContain('MegaMan'),
        expect( resp ).toContain('Ultron');

    });
});