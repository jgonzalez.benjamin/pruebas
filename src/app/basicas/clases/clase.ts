
export class Jugador{

    hp:number;
    armas:any[];
    nick:string;

    constructor(){
        this.hp = 100;
        this.nick = 'Jugador 1';
        this.armas = ['sin armas'];
    }
    resibeDanio( danio: number ){
        if (danio >= this.hp) {
            this.hp = 0;
        }else{
            this.hp = this.hp - danio;
        }
        return this.hp;
    }
    setNick( nick:string ){
        this.nick = nick;
    }
    setArmas( armas:any[] ){
        this.armas.push( armas );
    }
    getNick():string {
        return this.nick;
    }

}