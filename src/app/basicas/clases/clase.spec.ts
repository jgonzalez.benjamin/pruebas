import { Jugador } from "./clase";

// Para saltar las pruebas hay que poner una x antes del 'it' o el 'describe';

xdescribe('Pruebas de clase', () => {

    /* 
        - beforeAll();
        - beforeEach();
        - afterAll();
        - afterEach();
    */

    it('Debe de retornar 80 de hp, si se recibe 20 de daño', ()=>{
        
        const jugador = new Jugador();
        let resp = jugador.resibeDanio(20);
        expect( resp ).toBe(80);
    });

    it('Debe de retornar 50 de hp, si se recibe 50 de daño', ()=>{

        const jugador = new Jugador();
        let resp = jugador.resibeDanio(50);
        expect( resp ).toBe(50);
    });

    it('Debe de retornar 0 de hp, si se recibe 100 o mas de daño', ()=>{
        
        const jugador = new Jugador();
        let resp = jugador.resibeDanio(100);
        expect( resp ).toBe(0);
    });

    it('Debe de retornar el nick del jugador ', ()=>{
        
        const jugador = new Jugador();
        let nick = 'Prueba Nick';
        jugador.setNick(nick);
        let resp = jugador.getNick();

        expect( resp ).toBe(nick);
    });
});