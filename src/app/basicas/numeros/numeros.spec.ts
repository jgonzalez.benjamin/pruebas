import { incrementar } from "./numeros";

describe('Pruebas de numeros', () => {

    it('Debe de retornar 100, si el numero es mayor a 100', () => {

        const numero: number = 300;
        const resp = incrementar( numero);

        expect( resp ).toBe(100);

    });

    xit('Debe de retornar el valor del parametro +1, si el numero es menor a 100', () => {

        const numero: number = 20;
        const resp = incrementar( numero);

        expect( resp ).toBe(21);

    });

});