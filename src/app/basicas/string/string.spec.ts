import { saludar } from "./string";

// describe('pruebas de strings');
// it(' debe de retornar un string ');

describe('Pruebas de strings', () => {

    it('debe de regresar un string', () => {
        const resp = saludar('Benja');

        expect( typeof resp ).toBe('string')// regresaria un true or false
    });
    it('debe de mostrar un mensaje con el valor del parametro', () => {
        const nombre = 'Benja';
        const resp = saludar(nombre);

        expect(resp).toContain(nombre);
    });
});