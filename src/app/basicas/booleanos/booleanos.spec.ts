import { usuarioLogeado } from "./booleanos";

describe('Pruebas de booleanos', () =>{
    it('debe de retornar true', ()=>{
        const resp = usuarioLogeado();
        expect( resp ).toBeTruthy();
        // se le puede aregar antes de tobeTruthy() el .not. para indicar que no va a ser verdadero.
        // o tambien se puede poner toBeFalsy() para indicar que va a ser falso.
    })
})